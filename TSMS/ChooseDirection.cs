﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Net.Http;
using System.Web.Script.Serialization;
namespace WindowsFormsApp3
{
    public partial class ChooseDirection : Form
    {
        public ChooseDirection()
        {
            InitializeComponent();
        }
/*
        class Json
        {
            public TimeDate time { get; set; }
        }
*/
        class TimeDate
        {
            public string abbreviation { get; set; }
            public string client_ip { get; set; }
            public string datetime { get; set; }
            public int day_of_week { get; set; }
            public int day_of_year { get; set; }
            public bool dst { get; set; }
            public object dst_from { get; set; }
            public int dst_offset { get; set; }
            public object dst_until { get; set; }
            public long raw_offset { get; set; }
            public string timezone { get; set; }
            public long unixtime { get; set; }
            public string utc_datetime { get; set; }
            public string utc_offset { get; set; }
            public int week_number { get; set; }
        }

        public DataTable DB(string cmdstr)
        {
            string constring = @"DATA SOURCE=localhost:1521/orcl;PASSWORD=emir;USER ID = EMIR";

            OracleConnection connection = new OracleConnection(constring);

            string commandString = cmdstr;


            OracleCommand command = new OracleCommand(commandString, connection);

            connection.Open();
            OracleDataReader dataReader = command.ExecuteReader();

            DataTable dataTable = new DataTable();
            dataTable.Load(dataReader);

            connection.Close();

            return dataTable;
            //            dataGridView1.DataSource = dataTable;
        }


        private async void Form1_LoadAsync(object sender, EventArgs e)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://worldtimeapi.org/api/");

            // find by timezone entered
//            HttpResponseMessage response = await client.GetAsync("timezone/Asia/Baku");
            // find by public ip
            HttpResponseMessage response = await client.GetAsync("ip");

            var result = await response.Content.ReadAsStringAsync();

            var JSON_OBJECT = new JavaScriptSerializer().Deserialize<TimeDate>(result);
            string dayOfWeek = JSON_OBJECT.datetime;
            long unixtime = JSON_OBJECT.unixtime;

            //            Console.WriteLine(dayOfWeek + "JS");

            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixtime).ToLocalTime();
            label2.Text = "Current IP Time: " + dtDateTime.ToString();
        }


        
        private void button1_Click(object sender, EventArgs e)
        {
            LoginT form2 = new LoginT();
            form2.ShowDialog();
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoginS form3 = new LoginS();
            form3.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
//            timer1.Start();
        }
    }
}

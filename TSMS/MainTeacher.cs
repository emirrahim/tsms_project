﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        string imgLocation;
        string select = "SELECT s.S_ID, s.S_NAME, s.S_SURNAME, s.S_AGE, su.SUBJECT_NAME, s.S_GRADE FROM STUDENT s INNER JOIN SUBJECT su ON s.SUBJECT_ID = su.SUBJECT_ID";
        bool flag = false;

        private void aDDSTUDENTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel2");

        }

        private void dELETESTUDENTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel3");

        }

        private void aLLSTUDENTSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel1");

            dataGridView1.DataSource = new Orcl().DB(select);
            dataGridView1.RowHeadersWidth = 30;
            dataGridView1.Width = dataGridView1.Columns.Count * dataGridView1.Columns[0].Width + 30;

            setHeaderText(dataGridView1);

        }

        private void Form4_Load(object sender, EventArgs e)
        {
            // Hide all panels
            foreach (Control c in this.Controls)
            {
                if (c is Panel) c.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // covert image to byte array
            //            Image img = pictureBox1.Image;
            //          byte[] arr;
            //      ImageConverter converter = new ImageConverter();
            //        arr = (byte[])converter.ConvertTo(img, typeof(byte[]));

            //command.CommandText = "INSERT INTO ImagesTable (Image) VALUES('" + arr + "')";

            //            new Orcl().DB("Insert into student values(a)");

            //Console.WriteLine(imgLocation + "IMGLOCATION");
            new Orcl().DB("INSERT INTO STUDENT VALUES(" + textBox1.Text + ", '" + textBox2.Text + "', '"
                                            + textBox3.Text + "', " + textBox4.Text + ", " + textBox5.Text + ", '" + imgLocation + "', NULL)");

            MessageBox.Show("New Student Added");
        //    textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = "";
        }


        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)13)
            {
                this.SelectNextControl(textBox1, true, true, true, true);
              e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                this.SelectNextControl(textBox2, true, true, true, true);
               e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                this.SelectNextControl(textBox3, true, true, true, true);
               e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                this.SelectNextControl(textBox4, true, true, true, true);
                e.Handled = true; // textboxu auto silmeyi saxlayir
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                this.SelectNextControl(textBox5, true, true, true, true);
                e.Handled = true; // textboxu auto silmeyi saxlayir
            }

        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            if(String.IsNullOrEmpty(textBox1.Text) || !textBox1.Text.All(char.IsDigit))
            {
                e.Cancel = true;
                MessageBox.Show("Please type correct ID");
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(textBox2.Text) || textBox2.Text.All(char.IsDigit))
            {
                e.Cancel = true;
                MessageBox.Show("Please type correct name");
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(textBox3.Text) || textBox3.Text.All(char.IsDigit))
            {
                e.Cancel = true;
                MessageBox.Show("Please type correct surname");
            }
            else
            {
                e.Cancel = false;
            }

        }

        private void textBox4_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(textBox4.Text) || !textBox4.Text.All(char.IsDigit))
            {
                e.Cancel = true;
                MessageBox.Show("Please type correct age");
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void textBox5_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(textBox4.Text) || !textBox4.Text.All(char.IsDigit))
            {
                e.Cancel = true;
                MessageBox.Show("Please type correct Subject ID");
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //  openFileDialog1.Multiselect = true;
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileNames[0]);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                imgLocation = openFileDialog1.FileNames[0]; //secilmis bir sekli gosterir path-ni
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataTable dt = new Orcl().DB(select + " WHERE s.S_ID = " + textBox6.Text);

            if (dt.Rows.Count != 0)
            {
                new Orcl().DB("DELETE FROM STUDENT WHERE S_ID =" + textBox6.Text);
                MessageBox.Show("STUDENT HAS BEEN DELETED");
                textBox6.Text = "";
            }
            else
            {
                MessageBox.Show("PLEASE ENTER CORRECT STUDENT ID");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DataTable dt = new Orcl().DB(select + " WHERE S_IMAGE = '" + imgLocation + "'");

            if (dt.Rows.Count != 0)
            {
                new Orcl().DB("DELETE FROM STUDENT  WHERE S_IMAGE = '" + imgLocation + "'");
                MessageBox.Show("STUDENT HAS BEEN DELETED");
                textBox6.Text = "";
            }
            else
            {
                MessageBox.Show("PLEASE ENTER CORRECT IMAGE");
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image = Image.FromFile(openFileDialog2.FileName);
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                imgLocation = openFileDialog2.FileName;
            }
        }

        private void sEARCHToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel4");


            dataGridView2.DataSource =  new Orcl().DB(select);
            dataGridView2.RowHeadersWidth = 20;

            setHeaderText(dataGridView2);
        }


        private void button17_Click(object sender, EventArgs e)
        {
//            dataGridView2.DataSource = new Orcl().DB(select + " Where Su.Subject_Name = (SELECT SUBJECT_NAME FROM SUBJECT GROUP BY SUBJECT_NAME HAVING SUBJECT_NAME = '" + textBox50.Text + "')");
            dataGridView2.DataSource = new Orcl().DB("Select * FROM Std_Tch_Subj Where Subject = '" + textBox50.Text.ToUpper() + "'");
        }

        private void button18_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = new Orcl().DB("SElECT * FROM Std_Tch_Subj WHERE SUBJECT_ID = (SELECT SUBJECT_ID FROM TEACHER WHERE TEACHER_ID = (SELECT PASS FROM AUTH))");
        }

        private void button6_Click(object sender, EventArgs e)
        {

            // default 1
            var selectedButtons = checkSelectedButton();
            int rb1 = selectedButtons.Item1;
            string rb2 = selectedButtons.Item2;

            switch (rb1)
            {
                case 2:
                    dataGridView2.DataSource = new Orcl().DB(select + " ORDER BY s.S_SURNAME " + rb2);
                    setHeaderText(dataGridView2);
                    break;
                case 3:
                    dataGridView2.DataSource = new Orcl().DB(select + " ORDER BY s.S_AGE " + rb2);
                    setHeaderText(dataGridView2);
                    break;
                case 4:
                    dataGridView2.DataSource = new Orcl().DB(select + " ORDER BY s.S_GRADE " + rb2);
                    setHeaderText(dataGridView2);
                    break;
                default:
                    dataGridView2.DataSource = new Orcl().DB(select + " ORDER BY s.S_NAME " + rb2);
                    setHeaderText(dataGridView2);
                    break;
            }
        }


        private void button7_Click(object sender, EventArgs e)
        {

            if (!String.IsNullOrEmpty(textBox7.Text))
            {
                dataGridView2.DataSource = new Orcl().DB(select + " WHERE s.S_ID =" + textBox7.Text);
                setHeaderText(dataGridView2);
            }
            else
                MessageBox.Show("Please Enter id");
        }

        private void cHANGEINFOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel6");


        }

        private void button10_Click(object sender, EventArgs e)
        {
            DataTable dt =  new Orcl().DB("SELECT S_NAME, S_SURNAME, S_AGE, S_IMAGE FROM STUDENT WHERE S_ID = " + textBox8.Text);

            if (dt.Rows.Count != 0)
            {
                string name = dt.Rows[0][0].ToString();
                string surname = dt.Rows[0][1].ToString();
                string age = dt.Rows[0][2].ToString();
                string image = dt.Rows[0][3].ToString();

                textBox9.Text = name;
                textBox10.Text = surname;
                textBox11.Text = age;

                pictureBox3.Image = Image.FromFile(image);
                pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;

            }
            else
            {
//                MessageBox.Show("PLEASE ENTER CORRECT ID");
            }


        }

        private void button9_Click(object sender, EventArgs e)
        {
            string name = textBox9.Text;
            string surname = textBox10.Text;
            string age = textBox11.Text;



//            DataTable dt = new DataTable();
 //           dt.Rows.Add(name);
   //         dt.Rows.Add(surname);
     //       dt.Rows.Add(age);

            new Orcl().DB("UPDATE STUDENT SET S_NAME = '" + name + "', S_SURNAME = '" + surname + "', S_AGE = " + Int32.Parse(age) + ", S_IMAGE = '" + imgLocation + "' WHERE S_ID = " + textBox8.Text);
            MessageBox.Show("Your Data Updated");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (openFileDialog3.ShowDialog() == DialogResult.OK)
            {
                pictureBox3.Image = Image.FromFile(openFileDialog3.FileName);
                pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
                imgLocation = openFileDialog3.FileName;
            }

        }

        private void aDDGRADEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel7");

        }


        private void button11_Click(object sender, EventArgs e)
        {
            if (new Orcl().DB("SELECT SUBJECT_ID, S_ID FROM STUDENT WHERE SUBJECT_ID = " + textBox15.Text + "AND S_ID = " + textBox13.Text).Rows.Count != 0)
            {
                flag = true;
                MessageBox.Show("STUDENT FOUND. PLEASE ADD GRADE");
            }
            else
            {
                flag = false;
                MessageBox.Show("PLEASE ENTER CORRECT DATA");
            }

        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (flag)
            {
                new Orcl().DB("UPDATE STUDENT SET S_GRADE = " + textBox14.Text + " WHERE SUBJECT_ID = " + textBox15.Text + " AND S_ID = " + textBox13.Text);

                MessageBox.Show("STUDENT GRADE ADDED");
            }
            else
            {
                MessageBox.Show("PLEASE ENTER CORRECT DATA");
            }
        }

        private void aLLSTUDENTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel8");

            printDialog1.AllowCurrentPage = true;
            printDialog1.AllowPrintToFile = true;
            printDialog1.AllowSelection = true;
            printDialog1.AllowSomePages = true;
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            dataGridView1.DataSource = new Orcl().DB(select);

            setHeaderText(dataGridView1);

            Bitmap bm = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            dataGridView1.DrawToBitmap(bm, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));
            e.Graphics.DrawImage(bm, 0, 0);

        }

        private void button14_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if(printDialog1.ShowDialog() == DialogResult.OK)
            {
                printDocument1.Print();
            }
        }

        private void sTUDENTToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PanelVisible("panel9");


            printDialog2.AllowCurrentPage = true;
            printDialog2.AllowPrintToFile = true;
            printDialog2.AllowSelection = true;
            printDialog2.AllowSomePages = true;

        }

        private void button15_Click(object sender, EventArgs e)
        {
            printPreviewDialog2.Document = printDocument2;
            printPreviewDialog2.Show();

        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (printDialog2.ShowDialog() == DialogResult.OK)
            {
                printDocument2.Print();
            }
        }

        private void printDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            dataGridView1.DataSource = new Orcl().DB("SELECT TEACHER_ID, TEACHER_NAME, TEACHER_SURNAME, SUBJECT_ID FROM TEACHER WHERE TEACHER_ID = (SELECT PASS FROM AUTH)");

            dataGridView1.Columns[0].HeaderText = "ID";
            dataGridView1.Columns[1].HeaderText = "NAME";
            dataGridView1.Columns[2].HeaderText = "SURNAME";
            dataGridView1.Columns[3].HeaderText = "SUBJECT";


            Bitmap bm = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            dataGridView1.DrawToBitmap(bm, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));
            e.Graphics.DrawImage(bm, 0, 0);

        }

        private void mYDATAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel10");

            textBox21.ReadOnly = textBox22.ReadOnly = textBox23.ReadOnly = textBox24.ReadOnly = true;

            string id = new Orcl().DB("SELECT PASS FROM AUTH").Rows[0][0].ToString();

            DataTable dt = new Orcl().DB("SELECT TEACHER_NAME, TEACHER_SURNAME , SUBJECT_ID FROM TEACHER WHERE TEACHER_ID = " + id);

            string name = dt.Rows[0][0].ToString();
            string surname = dt.Rows[0][1].ToString();
            string subject = dt.Rows[0][2].ToString();

            textBox21.Text = id;
            textBox22.Text = name;
            textBox23.Text = surname;
            textBox24.Text = subject;

        }

        private void button19_Click(object sender, EventArgs e)
        {
            string id = new Orcl().DB("SELECT PASS FROM AUTH").Rows[0][0].ToString();
            new Orcl().DB("UPDATE TEACHER SET TEACHER_NAME = '" + textBox17.Text + "', TEACHER_SURNAME = '" + textBox18.Text + "', SUBJECT_ID = " + Int32.Parse(textBox19.Text) + " WHERE TEACHER_ID = " + id);
            MessageBox.Show("YOUR DATA HAS BEEN UPDATED");
        }


        private void uPDATEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel11");

        }

        private void PanelVisible(string panelName)
        {
            foreach (var panel in this.Controls.OfType<Panel>())
            {
                // show only selected panel
                panel.Visible = panel.Name == panelName;
                panel.Location = new Point(0, 30);
                panel.Size = new Size(this.Width, this.Height);


                //                panel.Location = new Point(
                //               this.ClientSize.Width / 2 - panel.Size.Width / 2,
                //             this.ClientSize.Height / 2 - panel.Size.Height / 2);
                //           panel.Anchor = AnchorStyles.None;
            }
        }


        private Tuple<int, string> checkSelectedButton()
        {
            int c = 0;
            string o = "ASC";

            if (radioButton1.Checked)
            {
                c = 1;
            }
            else if (radioButton2.Checked)
            {
                c = 2;
            }
            else if (radioButton3.Checked)
            {
                c = 3;
            }
            else if (radioButton4.Checked)
            {
                c = 4;
            }

            if (radioButton6.Checked)
            {
                o = "DESC";
            }

            return Tuple.Create(c, o);

        }

        private void setHeaderText(DataGridView dataGridView)
        {
            dataGridView.Columns[0].HeaderText = "ID";
            dataGridView.Columns[1].HeaderText = "NAME";
            dataGridView.Columns[2].HeaderText = "SURNAME";
            dataGridView.Columns[3].HeaderText = "AGE";
            dataGridView.Columns[4].HeaderText = "SUBJECT";
            dataGridView.Columns[5].HeaderText = "GRADE";

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

﻿namespace WindowsFormsApp3
{
    partial class MainStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mYDATAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uPDATEMYINFOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mYDATAToolStripMenuItem,
            this.uPDATEMYINFOToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // mYDATAToolStripMenuItem
            // 
            this.mYDATAToolStripMenuItem.Name = "mYDATAToolStripMenuItem";
            this.mYDATAToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.mYDATAToolStripMenuItem.Text = "MY ALL INFO";
            this.mYDATAToolStripMenuItem.Click += new System.EventHandler(this.mYDATAToolStripMenuItem_Click);
            // 
            // uPDATEMYINFOToolStripMenuItem
            // 
            this.uPDATEMYINFOToolStripMenuItem.Name = "uPDATEMYINFOToolStripMenuItem";
            this.uPDATEMYINFOToolStripMenuItem.Size = new System.Drawing.Size(113, 20);
            this.uPDATEMYINFOToolStripMenuItem.Text = "UPDATE MY INFO";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.NavajoWhite;
            this.panel10.Controls.Add(this.label2);
            this.panel10.Controls.Add(this.textBox37);
            this.panel10.Controls.Add(this.label1);
            this.panel10.Controls.Add(this.pictureBox1);
            this.panel10.Controls.Add(this.label32);
            this.panel10.Controls.Add(this.label34);
            this.panel10.Controls.Add(this.label35);
            this.panel10.Controls.Add(this.label36);
            this.panel10.Controls.Add(this.textBox30);
            this.panel10.Controls.Add(this.textBox31);
            this.panel10.Controls.Add(this.textBox32);
            this.panel10.Controls.Add(this.textBox33);
            this.panel10.Controls.Add(this.textBox25);
            this.panel10.Controls.Add(this.label37);
            this.panel10.Location = new System.Drawing.Point(0, 44);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(190, 406);
            this.panel10.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 343);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "GRADE";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(310, 340);
            this.textBox37.Multiline = true;
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(143, 21);
            this.textBox37.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(590, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "IMAGE";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(523, 174);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(183, 136);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(252, 127);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(18, 13);
            this.label32.TabIndex = 20;
            this.label32.Text = "ID";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(233, 297);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(55, 13);
            this.label34.TabIndex = 19;
            this.label34.Text = "SUBJECT";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(233, 237);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 13);
            this.label35.TabIndex = 18;
            this.label35.Text = "SURNAME";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(243, 179);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(38, 13);
            this.label36.TabIndex = 17;
            this.label36.Text = "NAME";
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(310, 125);
            this.textBox30.Multiline = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(143, 19);
            this.textBox30.TabIndex = 13;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(310, 174);
            this.textBox31.Multiline = true;
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(143, 23);
            this.textBox31.TabIndex = 10;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(310, 234);
            this.textBox32.Multiline = true;
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(143, 22);
            this.textBox32.TabIndex = 9;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(310, 294);
            this.textBox33.Multiline = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(143, 21);
            this.textBox33.TabIndex = 8;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(4, -26);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(143, 23);
            this.textBox25.TabIndex = 7;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label37.Location = new System.Drawing.Point(367, 38);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(83, 22);
            this.label37.TabIndex = 0;
            this.label37.Text = "MY INFO";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.BurlyWood;
            this.panel11.Controls.Add(this.button1);
            this.panel11.Controls.Add(this.pictureBox2);
            this.panel11.Controls.Add(this.label29);
            this.panel11.Controls.Add(this.label30);
            this.panel11.Controls.Add(this.label31);
            this.panel11.Controls.Add(this.button19);
            this.panel11.Controls.Add(this.textBox34);
            this.panel11.Controls.Add(this.textBox35);
            this.panel11.Controls.Add(this.textBox36);
            this.panel11.Controls.Add(this.textBox20);
            this.panel11.Controls.Add(this.label33);
            this.panel11.Location = new System.Drawing.Point(246, 71);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(185, 379);
            this.panel11.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(462, 307);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "BROWSE IMAGE";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(430, 145);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(213, 141);
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(198, 266);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 13);
            this.label29.TabIndex = 19;
            this.label29.Text = "AGE";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(180, 206);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(61, 13);
            this.label30.TabIndex = 18;
            this.label30.Text = "SURNAME";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(190, 148);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 17;
            this.label31.Text = "NAME";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(257, 307);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(143, 23);
            this.button19.TabIndex = 12;
            this.button19.Text = "UPDATE";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(257, 143);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(143, 23);
            this.textBox34.TabIndex = 10;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(257, 203);
            this.textBox35.Multiline = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(143, 22);
            this.textBox35.TabIndex = 9;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(257, 263);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(143, 21);
            this.textBox36.TabIndex = 8;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(4, -26);
            this.textBox20.Multiline = true;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(143, 23);
            this.textBox20.TabIndex = 7;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.label33.Location = new System.Drawing.Point(347, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(162, 22);
            this.label33.TabIndex = 0;
            this.label33.Text = "UPDATE MY INFO";
            // 
            // MainStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainStudent";
            this.Text = "Form5";
            this.Load += new System.EventHandler(this.MainStudent_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mYDATAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uPDATEMYINFOToolStripMenuItem;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox37;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class LoginS : Form
    {
        public LoginS()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Orcl().DB("DELETE FROM AUTH WHERE EXISTS (SELECT COUNT(*) FROM AUTH HAVING count(*) > 0)");
            new Orcl().DB("INSERT INTO AUTH(PASS) VALUES(" + textBox2.Text + ")");


            bool b = checkAuthDb(textBox1.Text, textBox2.Text);
            //  label4.Text = b.ToString();
            if (!b)
                label4.Text = "Invalid Password or Username";
            else
            {
                new MainStudent().ShowDialog();
            }

        }

        private bool checkAuthDb(string name, string pass)
        {

            DataTable dt = new Orcl().DB("Select * from STUDENT");


            foreach (DataRow dataRow in dt.Rows)
            {
                if (dataRow["S_NAME"].ToString() == name && dataRow["S_ID"].ToString() == pass)
                {
                    return true;
                }
            }
            return false;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)  // AutoSelect next control on Enter key press
            {
                this.SelectNextControl(textBox1, true, true, true, true);
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                this.SelectNextControl(textBox2, true, true, true, true);
                e.Handled = true;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            new Mail().sendMail();
            //MessageBox.Show("Your password has been send to mail");
        }

        private void LoginS_Load(object sender, EventArgs e)
        {

        }

    }
}

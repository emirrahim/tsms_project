﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class MainStudent : Form
    {
        public MainStudent()
        {
            InitializeComponent();
        }

        string imgLocation;
        string select = "SELECT s.S_NAME, s.S_SURNAME, s.S_IMAGE, su.SUBJECT_NAME, s.S_GRADE FROM STUDENT s INNER JOIN SUBJECT su ON s.SUBJECT_ID = su.SUBJECT_ID";

        private void mYDATAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelVisible("panel10");
            panel10.Size = new Size(this.Width, this.Height);

            textBox30.ReadOnly = textBox31.ReadOnly = textBox32.ReadOnly = textBox33.ReadOnly = textBox37.ReadOnly = true;

            string id = new Orcl().DB("SELECT PASS FROM AUTH").Rows[0][0].ToString();

            DataTable dt = new Orcl().DB(select + " WHERE s.S_ID = " + id);

            string name = dt.Rows[0][0].ToString();
            string surname = dt.Rows[0][1].ToString();
            string image = dt.Rows[0][2].ToString();
            string subject = dt.Rows[0][3].ToString();
            string grade = dt.Rows[0][4].ToString();

            textBox30.Text = id;
            textBox31.Text = name;
            textBox32.Text = surname;
            textBox33.Text = subject;
            textBox37.Text = grade;

            pictureBox1.Image = Image.FromFile(image);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

        }


        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            PanelVisible("panel11");
            panel11.Size = new Size(this.Width, this.Height);

            string id = new Orcl().DB("SELECT PASS FROM AUTH").Rows[0][0].ToString();

            DataTable dt = new Orcl().DB("Select S_NAME, S_SURNAME, S_AGE, S_IMAGE FROM STUDENT WHERE S_ID = " + id);

            string name = dt.Rows[0][0].ToString();
            string surname = dt.Rows[0][1].ToString();
            string age = dt.Rows[0][2].ToString();
            string image = dt.Rows[0][3].ToString();

            textBox34.Text = name;
            textBox35.Text = surname;
            textBox36.Text = age;
            pictureBox2.Image = Image.FromFile(image);

        }

        private void button19_Click(object sender, EventArgs e)
        {
            string id = new Orcl().DB("SELECT PASS FROM AUTH").Rows[0][0].ToString();

            new Orcl().DB("UPDATE STUDENT SET S_NAME = '" + textBox34.Text + "', S_SURNAME = '" + textBox35.Text + "', S_AGE = " + Int32.Parse(textBox36.Text) + ", S_IMAGE = '" + imgLocation + "'" + " WHERE S_ID = " + id);

            MessageBox.Show("YOUR DATA HAS BEEN UPDATED");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);
                pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
                imgLocation = openFileDialog1.FileName;
            }
        }

        private void PanelVisible(string panelName)
        {
            foreach (var panel in this.Controls.OfType<Panel>())
            {
                // show only selected panel
                panel.Visible = panel.Name == panelName;
                panel.Location = new Point(0, 30);
            }
        }

        private void MainStudent_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is Panel) c.Visible = false;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}

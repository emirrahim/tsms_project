﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;

namespace WindowsFormsApp3
{
    class Orcl
    {
        public DataTable DB(string cmdstr)
        {
            string constring = @"DATA SOURCE=localhost:1521/orcl;PASSWORD=emir;USER ID = EMIR";

            OracleConnection connection = new OracleConnection(constring);


            OracleCommand command = new OracleCommand(cmdstr, connection);

            connection.Open();
            OracleDataReader dataReader = command.ExecuteReader();

            DataTable dataTable = new DataTable();
            dataTable.Load(dataReader);

            connection.Close();

            return dataTable;
        }
    }
}
